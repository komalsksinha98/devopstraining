variable "prefix" {default = "ust-devops"}
variable "name" {default = "ust"}
variable "location" {default = "West Europe"}

variable "name_count" {default = ["server1", "server2", "server3"] }